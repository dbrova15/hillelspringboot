package com.hillel.app.repository;

import com.hillel.app.Entities.ClientEntity;
import com.hillel.app.Entities.ClientSubscriptionEntity;
import com.hillel.app.Entities.SubscriptionEntity;
import com.hillel.app.Entities.VisitEntity;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.List;

@Repository
@RequiredArgsConstructor
@Slf4j
public class Repo {
    private static final String INSERT_CLIENT_NAME_FROM_CLIENT = "INSERT INTO client (name, date) SELECT '%s', NOW() WHERE '%s' NOT IN SELECT name FROM client);";
    public static final String SELECT_ALL_FROM_SUBSCRIPTION = "SELECT id, name, time, cost FROM subscription;";
    public static final String SELECT_LAST_GYM_IN = "SELECT id, datein, dateout, time, client_subscription from visit WHERE client_subscription = %s order by datein limit 1";
    public static final String SELECT_CLIENT_SUBSCRIPTION = "select subscription, client, date_of_purchase, state from client_subscription where client = %s";
    public static final String SELECT_CLIENT_SUBSCRIPTION_ACTIVE = "select subscription, client, date_of_purchase, state from client_subscription where client = %s and state is true";
    public static final String UPDATE_DATEOUT_AND_TIME_IS_NULL = "UPDATE visit SET dateout = NOW(), time =(EXTRACT(EPOCH FROM (NOW() - timestamp '%s')::INTERVAL)/60) WHERE client_subscription =%s and dateout is NULL;";
    public static final String GYM_IN_SQL = "INSERT INTO visit (datein, client_subscription) VALUES ( NOW(), %s);";
    public static final String SELECT_CLT_SUBSCRIPTION_FROM_VISIT_WHERE_CLT_SUBSCRIPTION = "SELECT id, datein, dateout, time, client_subscription from visit WHERE client_subscription = %s";
    public static final String BUY_SUBSCRIPTION_SQL = "INSERT INTO client_subscription (subscription, client, date_of_purchase, state) SELECT '%s', '%s', NOW(), true;";
    public static final String SELECT_SUBSCRIPTION_WHERE_ID = "select id, time, cost from subscription where id = %s;";
    public static final String SELECT_STATE_CLIENT_SUBSCRIPTION = "select state from client_subscription where client = %s;";
    public static final String SELECT_CLIENT = "select name, date from client;";

    private final JdbcTemplate jdbcTemplate;

    public void insertClient(String name) {
        jdbcTemplate.update(INSERT_CLIENT_NAME_FROM_CLIENT, name, name);
    }

    public List<SubscriptionEntity> getSubscription() {
        return jdbcTemplate.query(
                SELECT_ALL_FROM_SUBSCRIPTION,
                new BeanPropertyRowMapper<>(SubscriptionEntity.class));
    }

    public List<ClientSubscriptionEntity> selectClientSubscription(Integer idClientSubscription) {
        return jdbcTemplate.query(
                String.format(SELECT_CLIENT_SUBSCRIPTION, idClientSubscription),
                new BeanPropertyRowMapper<>(ClientSubscriptionEntity.class));
    }

    public List<ClientSubscriptionEntity> getClientSubscriptionActive(Integer idClientSubscription) {
        return jdbcTemplate.query(
                String.format(SELECT_CLIENT_SUBSCRIPTION_ACTIVE, idClientSubscription),
                new BeanPropertyRowMapper<>(ClientSubscriptionEntity.class));
    }

    public void gymInUpdate(Integer clientSubscriptionListId) {
        jdbcTemplate.update(String.format(GYM_IN_SQL, clientSubscriptionListId));
    }

    public List<VisitEntity> getVisitRows(Integer clientSubscriptionListId) {
        return  jdbcTemplate.query(
                String.format(SELECT_CLT_SUBSCRIPTION_FROM_VISIT_WHERE_CLT_SUBSCRIPTION,
                        clientSubscriptionListId), new BeanPropertyRowMapper<>(VisitEntity.class));
    }

    public List<VisitEntity> getLastGymIn(Integer clientSubscriptionListId) {
        return jdbcTemplate.query(
                String.format(SELECT_LAST_GYM_IN, clientSubscriptionListId), new BeanPropertyRowMapper<>(VisitEntity.class));
    }

    public void gymOutUpdate(Integer clientSubscriptionListId, String dateIn) {
        jdbcTemplate.update(
                String.format(UPDATE_DATEOUT_AND_TIME_IS_NULL, dateIn, clientSubscriptionListId));
    }

    public void buySubscriptions(Integer idClient, Integer iDsubs) {
        jdbcTemplate.update(String.format(BUY_SUBSCRIPTION_SQL, idClient, iDsubs));
    }

    public List<SubscriptionEntity> getSubscriptions(Integer idSub) {
        return jdbcTemplate.query(String.format(SELECT_SUBSCRIPTION_WHERE_ID, idSub),
                new BeanPropertyRowMapper<>(SubscriptionEntity.class));
    }

    public List<ClientSubscriptionEntity> getStateClientSubscriptions(Integer idClient) {
        return jdbcTemplate.query(String.format(SELECT_STATE_CLIENT_SUBSCRIPTION, idClient),
                new BeanPropertyRowMapper<>(ClientSubscriptionEntity.class));
    }

    public List<ClientEntity> getClients() {
        return jdbcTemplate.query(SELECT_CLIENT,
                new BeanPropertyRowMapper<>(ClientEntity.class));
    }

//    @PostConstruct
//    public void init() {
//        List<ClientEntity> entities = getClients();
//
//        for (int i = 0; i < entities.size(); i++) {
//            log.info("Entity: " + entities.get(i));
//        }
//    }
}
