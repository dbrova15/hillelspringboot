package com.hillel.app.Utils;

import com.hillel.app.DTO.SubscriptionDTO;
import com.hillel.app.repository.Repo;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class SubscriptionUtils {
    @Autowired
    Repo repo;

    public List<SubscriptionDTO> getSubscription() {
        return MyDTOMapper.subscriptionDTOMapper(repo.getSubscription());
    }

    public String buySubscriptions(Integer idClient, Integer iDsubs) {
        ClientSubscriptionsUtils clientSubscriptionsUtils = new ClientSubscriptionsUtils();
        if (!clientSubscriptionsUtils.getStateClientSubscriptions(idClient)) {
            repo.buySubscriptions(idClient, iDsubs);
            return "Абонимент приобретён";
        } return "Уже есть действующий абонемент";

    }

    public SubscriptionDTO getSubscriptions(Integer idSub) {
        return MyDTOMapper.subscriptionDTOMapper(repo.getSubscriptions(idSub)).get(0);
    }
}
