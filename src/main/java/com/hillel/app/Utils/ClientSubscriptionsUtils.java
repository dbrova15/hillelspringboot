package com.hillel.app.Utils;

import com.hillel.app.DTO.ClientSubscriptionDTO;
import com.hillel.app.DTO.SubscriptionDTO;
import com.hillel.app.DTO.VisitDTO;
import com.hillel.app.repository.Repo;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class ClientSubscriptionsUtils {
    @Autowired
    Repo repo;
    
    boolean getStateClientSubscriptions(Integer idClient) {
        List<ClientSubscriptionDTO> clientSubs = MyDTOMapper.clientSubscriptionDTOMapper(repo.getStateClientSubscriptions(idClient));
        return clientSubs.size() != 0 ? clientSubs.get(0).getState(): false;
    }

    public int getTimeClientSubscriptions(Integer idClientSubscription) {
        VisitUtils visitUtils = new VisitUtils();
        SubscriptionUtils subscriptionUtils = new SubscriptionUtils();
        // Из таблицы визитов достать сумарное время всех посещений минус время по абонементу
        Integer timeVisits = 0;
        List<VisitDTO> visitList = visitUtils.getVisitRows(idClientSubscription);
        for (VisitDTO visit : visitList) {timeVisits += visit.getTime();}

        SubscriptionDTO subs = subscriptionUtils.getSubscriptions(
                getClientSubscriptionActive(idClientSubscription).getSubscription());
        return subs.getTime() - timeVisits;

    }

    public ClientSubscriptionDTO getClientSubscriptionActive(Integer idClientSubscription) {
        List<ClientSubscriptionDTO> clientSubscriptionActiveList = MyDTOMapper.clientSubscriptionDTOMapper(
                repo.getClientSubscriptionActive(idClientSubscription));
        return clientSubscriptionActiveList.size() != 0 ? clientSubscriptionActiveList.get(0): null;
    }


    public List<ClientSubscriptionDTO> getClientSubscriptionList(Integer idClientSubscription) {
        return MyDTOMapper.clientSubscriptionDTOMapper(repo.selectClientSubscription(idClientSubscription));
    }
}
