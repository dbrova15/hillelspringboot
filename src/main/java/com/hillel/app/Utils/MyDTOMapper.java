package com.hillel.app.Utils;

import com.hillel.app.DTO.ClientDTO;
import com.hillel.app.DTO.ClientSubscriptionDTO;
import com.hillel.app.DTO.SubscriptionDTO;
import com.hillel.app.DTO.VisitDTO;
import com.hillel.app.Entities.ClientEntity;
import com.hillel.app.Entities.ClientSubscriptionEntity;
import com.hillel.app.Entities.SubscriptionEntity;
import com.hillel.app.Entities.VisitEntity;

import java.util.ArrayList;
import java.util.List;


public class MyDTOMapper {

    public static List<SubscriptionDTO> subscriptionDTOMapper(List<SubscriptionEntity> subscriptions) {

        List<SubscriptionDTO> subscriptionDTOList = new ArrayList<>();

        for (SubscriptionEntity subscription : subscriptions) {
            subscriptionDTOList.add(new SubscriptionDTO(subscription));
        }
        return subscriptionDTOList;
    }

    public static List<VisitDTO> visitDTOMapper(List<VisitEntity> visitEntities) {

        List<VisitDTO> subscriptionDTOList = new ArrayList<>();

        for (VisitEntity visit : visitEntities) {
            subscriptionDTOList.add(new VisitDTO(visit));
        }
        return subscriptionDTOList;
    }

    public static List<ClientSubscriptionDTO> clientSubscriptionDTOMapper(List<ClientSubscriptionEntity> visitEntities) {

        List<ClientSubscriptionDTO> subscriptionDTOList = new ArrayList<>();

        for (ClientSubscriptionEntity clientSubscriptionEntity : visitEntities) {
            subscriptionDTOList.add(new ClientSubscriptionDTO(clientSubscriptionEntity));
        }
        return subscriptionDTOList;
    }

    public static List<ClientDTO> clientDTOMapper(List<ClientEntity> entity) {

        List<ClientDTO> clientDTOList = new ArrayList<>();

        for (ClientEntity clientEntity : entity) {
            clientDTOList.add(new ClientDTO(clientEntity));
        }
        return clientDTOList;
    }
}
