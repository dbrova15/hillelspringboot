package com.hillel.app.Utils;

import com.hillel.app.DTO.ClientDTO;
import com.hillel.app.repository.Repo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
public class ClientUtils {
    @Autowired
    Repo repo;

    public List<ClientDTO> getClients() {
        List<ClientDTO> clientDTOList = MyDTOMapper.clientDTOMapper(repo.getClients());
        return clientDTOList;
    }

    public void newClient(String name) {
        repo.insertClient(name);
    }


}
