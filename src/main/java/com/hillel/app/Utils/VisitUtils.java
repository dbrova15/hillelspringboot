package com.hillel.app.Utils;

import com.hillel.app.DTO.VisitDTO;
import com.hillel.app.repository.Repo;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class VisitUtils {
    @Autowired
    Repo repo;

    public List<VisitDTO> getVisitRows(Integer clientSubscriptionListId) {
        return  MyDTOMapper.visitDTOMapper(repo.getVisitRows(clientSubscriptionListId));
    }

    public VisitDTO getLastGymIn(Integer clientSubscriptionListId) {
        List<VisitDTO> vesitList = MyDTOMapper.visitDTOMapper(repo.getLastGymIn(clientSubscriptionListId));
        return vesitList.size() != 0 ? vesitList.get(0): null;
    }

    public String gymOut(Integer clientSubscriptionListId) {
        VisitDTO visit = getLastGymIn(clientSubscriptionListId);
        if (visit != null && visit.getDateOut() == null ) {
            repo.gymOutUpdate(clientSubscriptionListId, visit.getDateIn().toString());
            return "Посещение завершено";
        } else {
            return "Начатых посещений не найдено";
        }
    }

    public String gymIn(Integer clientSubscriptionListId) {
        VisitDTO lastGymIn = getLastGymIn(clientSubscriptionListId);
        if (lastGymIn == null || lastGymIn.getDateOut() != null ) {
            repo.gymInUpdate(clientSubscriptionListId);
            return "Посещение начато";
        } else {
            return "Есть не завершенное посещение";
        }

    }
}
