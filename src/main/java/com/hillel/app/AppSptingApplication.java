package com.hillel.app;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication
public class AppSptingApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppSptingApplication.class, args);
    }

//    @PostConstruct
//    public void init() {
//        ClientUtils clientUtils = new ClientUtils();
//        List<ClientDTO> clientDTOList = clientUtils.getClients();
//
//        for (int i = 0; i < clientDTOList.size(); i++) {
//            log.info("clientDTO: " + clientDTOList.get(i));
//        }
//    }

}
