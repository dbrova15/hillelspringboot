package com.hillel.app.Entities;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.Date;

@Setter
@Getter
@Accessors(chain = true)
public class ClientEntity {
    private Integer id;
    private String name;
    private Date date;
}
