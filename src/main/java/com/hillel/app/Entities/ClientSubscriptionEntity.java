package com.hillel.app.Entities;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.Date;

@Setter
@Getter
@Accessors(chain = true)
public class ClientSubscriptionEntity {
    private Integer id;
    private Integer subscription;
    private Integer client;
    private Date date_of_purchase;
    private Boolean state;
}
