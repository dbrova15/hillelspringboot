package com.hillel.app.Entities;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.time.LocalTime;
import java.time.LocalDateTime;

@Setter
@Getter
@Accessors(chain = true)
public class VisitEntity {
    private Integer id;
    private LocalDateTime dateIn;
    private LocalDateTime dateOut;
    private Integer time;
    private Integer client_subscription;
}
