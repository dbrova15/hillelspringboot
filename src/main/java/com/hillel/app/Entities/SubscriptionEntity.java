package com.hillel.app.Entities;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Setter
@Getter
@Accessors(chain = true)
public class SubscriptionEntity {
    private Integer id;
    private String name;
    private Integer time;
    private Double cost;
}
