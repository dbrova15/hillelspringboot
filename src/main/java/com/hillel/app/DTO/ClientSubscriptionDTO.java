package com.hillel.app.DTO;

import com.hillel.app.Entities.ClientSubscriptionEntity;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

// Клиентский абонемент (абонемент, клиент, дата покупки, состояние(открыт/закрыт))
@Data
@Accessors(chain = true)
public class ClientSubscriptionDTO {
    private Integer id;
    private Integer subscription;
    private Integer client;
    private Date date_of_purchase;
    private Boolean state;

    public ClientSubscriptionDTO(ClientSubscriptionEntity entity) {
        subscription = entity.getSubscription();
        client = entity.getClient();
        date_of_purchase = entity.getDate_of_purchase();
        state = entity.getState();
    }
}
