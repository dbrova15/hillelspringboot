package com.hillel.app.DTO;

import com.hillel.app.Entities.SubscriptionEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.Accessors;

// абонимент
@Data
@AllArgsConstructor
public class SubscriptionDTO {
    private Integer id;
    private String name;
    private Integer time;
    private Double cost;

    public SubscriptionDTO(SubscriptionEntity subscription) {
        id = subscription.getId();
        name = subscription.getName();
        time = subscription.getTime();
        cost = subscription.getCost();
    }
}
