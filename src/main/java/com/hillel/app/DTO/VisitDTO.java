package com.hillel.app.DTO;

import com.hillel.app.Entities.SubscriptionEntity;
import com.hillel.app.Entities.VisitEntity;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalTime;
import java.time.LocalDateTime;

// Посещение (дата и время входа, дата и время выхода, общее время, абонемент)
@Data
@Accessors(chain = true)
public class VisitDTO {
    private Integer id;
    private LocalDateTime dateIn;
    private LocalDateTime dateOut;
    private Integer time;
    private Integer client_subscription;

    public VisitDTO(VisitEntity visit) {
        dateIn = visit.getDateIn();
        dateOut = visit.getDateOut();
        time = visit.getTime();
        client_subscription = visit.getClient_subscription();
    }
}
