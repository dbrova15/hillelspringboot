package com.hillel.app.DTO;

import com.hillel.app.Entities.ClientEntity;
import com.hillel.app.Entities.ClientSubscriptionEntity;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@Accessors(chain = true)
public class ClientDTO {
    private Integer id;
    private String name;
    private Date date;

    public ClientDTO(ClientEntity entity) {
        this.name = entity.getName();
        this.date = entity.getDate();
    }
}
