package com.hillel.app.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PingController {
    //    Получение списка клиентов
    @GetMapping(value = "ping", headers = "Accept=application/json")
    public String ping() {
        return new String("{\"status\": \"OK\"}");
    }
}
