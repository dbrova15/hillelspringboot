package com.hillel.app.controllers;

import com.hillel.app.DTO.ClientDTO;
import com.hillel.app.repository.Repo;
import com.hillel.app.Utils.ClientUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ClientController {
    @Autowired
    Repo repo;

    private final ClientUtils clientUtils = new ClientUtils();

    //    Регистрация в клубе
    @PostMapping(value = "registration", headers = "Accept=application/json")
    public ResponseEntity<Object> registration(@RequestBody String nameClient) {
        clientUtils.newClient(nameClient);
        return ResponseEntity.ok().build();
    }

    //    Получение списка клиентов
    @GetMapping(value = "getClients", headers = "Accept=application/json")
    public List<ClientDTO> getClients() {
//        return MyDTOMapper.clientDTOMapper(repo.getClients());
        return clientUtils.getClients();
    }
}