package com.hillel.app.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hillel.app.Utils.VisitUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class VisitController {


    private final VisitUtils visitUtils = new VisitUtils();


    //Вход в зал (проверка осталось ли время в абонементе (время в абонементе - суммарное время всех посещений),
    // если нет - ошибка, если да - создаем сеанс и записываем начальное время)
    @PostMapping(value = "gymIn", headers = "Accept=application/json")
    public JsonNode gymIn(@RequestBody Integer idClient) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readTree("{result: " + visitUtils.gymIn(idClient) + "}");
    }

    //Выход из зала - находим активный сеанс по имени пользователя (если такого нет - ошибка) фиксируем время выхода,
    // насчитываем время прибывания и обновляем запись посещения
    @PostMapping(value = "gymOut", headers = "Accept=application/json")
    public JsonNode gymOut(@RequestBody Integer idClient) throws JsonProcessingException {

        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readTree("{result: " + visitUtils.gymOut(idClient) + "}");
    }
}
