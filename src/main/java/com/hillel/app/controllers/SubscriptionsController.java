package com.hillel.app.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hillel.app.DTO.SubscriptionDTO;
import com.hillel.app.Utils.SubscriptionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class SubscriptionsController {

    private final SubscriptionUtils subscriptionUtils = new SubscriptionUtils();
    //    Получение списка доступных для покупки абонементов
    @GetMapping(value = "getSubscriptions", headers = "Accept=application/json")
    public List<SubscriptionDTO> getSubscriptions() {
        return subscriptionUtils.getSubscription();
    }

    //    Покупка абонемента (Выбор из списка доступных, если есть не закончившийся - покупать новый нельзя)
    @PostMapping(value = "buySubscriptions", headers = "Accept=application/json")
    public JsonNode buySubscriptions(@RequestBody Integer idClient, Integer idSubs) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readTree("{result: " + subscriptionUtils.buySubscriptions(idClient, idSubs) + "}");}

}
