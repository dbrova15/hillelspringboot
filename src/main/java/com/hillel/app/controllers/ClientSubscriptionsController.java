package com.hillel.app.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hillel.app.Utils.ClientSubscriptionsUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ClientSubscriptionsController {
    private final ClientSubscriptionsUtils clientSubscriptionsUtils = new ClientSubscriptionsUtils();

    // Получить оставшееся время
    @PostMapping(value = "getTimeSubscriptions", headers = "Accept=application/json")
    public JsonNode getTimeClientSubscriptions(@RequestBody Integer idClientSubscription) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readTree("{time: " + clientSubscriptionsUtils.getTimeClientSubscriptions(idClientSubscription) + "}");
    }
}
